@extends('layout.master')

@section('content')

<div class="card shadow-sm">
    <div class="row align-items-center px-2 pt-2 pb-2 align-items-center">
        <div class="col-1">
            <img src=@yield('foto') alt="profile" width="40px"/>
        </div>
        <div class="col-4">
            <p class="card-title ">@yield('username')</p>
        </div>
      </div>
    
    <img src=@yield('imagePost') alt="" class="card-img-top">
    <div class="card-body">     
      <a href="" class="btn btn-outline-danger btn-sm"><i class="far fa-heart"></i></a>
      <p class="card-text">@yield('textPost')</p>

      <div class="input-group mb-3">
        <textarea class="form-control" placeholder="Tambah Komentar" id="floatingTextarea">@yield('komen')</textarea>
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button">Kirim</button>
        </div>
      </div>
    </div>
   </div>
@endsection